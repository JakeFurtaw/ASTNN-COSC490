# ASTNN - An Equivalent Mutant Identifier
An **Abstract Syntax Tree Neural Network** aiming to automatically identify the equivalent mutants within a dataset of mutants. 
- The `/parser` folder isn't necessary and was used for additional preprocessing on our dataset before integrating it into the pipeline.

## Requirements
```bash
  python                    3.10
  pandas                    2.0.0
  gensim                    4.3.1
  scikit-learn              1.2.2
  torch                     2.0.0
  pycparser                 2.21
  javalang                  0.13
  accelerate                0.19.0
  notebook                  6.5.4
  matplotlib                3.7.1
  numpy                     1.23.5
  pip                       23.0.1
  rdflib                    6.3.2
  torch                     2.0.0
  torchaudio                2.0.1
  torchvision               0.15.1
  transformers              4.28.0
  xlsxwriter                3.1.0
  tensorflow                2.12.0
```
## Mac Specific Requirements
```bash
  tensorflow-macos          2.12.0
  tensorflow-metal          0.8.0
```
> Note that the original ASTNN implementation, from which this was forked, utilized Python 3.6 and older library versions. We have updated the project to Python 3.10 and more recent library versions.

## Deliverables
- Implemented a [mutant dataset](https://b2share.eudat.eu/records/fd8e674385214fe9a327941525c31f53) from [Mutantbench](https://github.com/MutantBench/MutantBench)
- Created a `/parser` which inserts each mutation into its original program and returns the mutated method/file.
- Updated ASTNN to Python 3.10 and more recent library versions
- Updated `pipeline.py` to support parsing of Java mutants using javalang
- Saved the trained model as a file to allow for inference, located in best_results/checkpoint-1188
- Added `test_from_trained.py` to get predictions on additional data using a saved trained model
- General refactoring of codebase to improve code quality
- Added learning rate hyperparameter
- Sequential model based on Code2Vec method embeddings (used for binary classification of equivalent mutants)
- Transformer model based on Code2Vec method embeddings (used for binary classification of EMs)
- codeBERT model based on natural language (used for binary classification of EMs)
- Implemented a new approach to equivalent mutant identification: using Code2Vec's code vectors with a neural network found in `model_c2v.py`
- Implemented a new training script `train_c2v.py` to support the new code vector based model
- Diagrams visualizing the process can be found in `diagrams` folder


## Breakdown
- `pipeline.py` is a modified pipeline to work with Java. Prepares data for the ASTNN
- `model.py` contains the origina ASTNN model
- `train` python files are currently modified to work with the modified ASTNN
- `test_from_trained.py` used to get predictions on additional data
- `model_c2v.py`contains modified ASTNN that works with method embeddings instead of block sequences
- `trees.py`,`utils.py`, `config.py` are used by `pipeline` or `train`
- `sequential.ipynb` Sequential model that uses Code2Vec method embeddings to perform binary classification of EMs/non-EMs
- `transformer.ipynb` Transformer model that uses Code2Vec method embeddings to perform binary classification of EMs/non-EMs
- `bert_regular.ipynb` codeBERT model that uses the original dataset with no modifications
- `bert_undersampled.ipynb` codeBERT model that undersamples the original dataset to match the amount of EMs/non-EMs
- `bert_oversampled.ipynb` codeBERT model that oversamples the training dataset for a 50/50 but keep the training and validation sets 85/15
- `ASTNN-Estimator-with-lr.ipynb` ASTNN estimator with learning rate implemented
- `data` contains data used in the original and modified ASTNNs
- `parser` contains the original programs with the mutants placed in, labeled dataset files in `csv` and `pkl` formats,
python files for the `data_parser`, method embeddings file from Code2Vec, original mutant dataset from MutantBench, and other python files that deal with data preprocessing
- `best_results` contains the best codeBERT model and html reports on performance
- `diagrams` contains diagrams visualizing different pipelines