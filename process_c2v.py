import csv
import random
import pickle
import pandas as pd

def main():
    with open("./c2v_dataset.vectors", "r") as file:
        data = []
        for idx, line in enumerate(file):
            numbers = line.strip().split()
            numbers = [float(num) for num in numbers]
            label = random.randint(1, 2)
            row = {"id": idx+1, "code": numbers, "label": label}
            data.append(row)

    df = pd.DataFrame(data)
    df.to_csv("c2v_processed.csv", index=False)
    
    with open("c2v_processed.pkl", "wb") as file:
        pickle.dump(data, file)

if __name__ == "__main__":
    main()
