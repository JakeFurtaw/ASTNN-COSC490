import os
import pickle
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from sklearn.model_selection import train_test_split
from model_c2v import BatchProgramClassifier, Code2VecEncoder
from sklearn.model_selection import KFold


# Load data
def load_data(file_path):
    with open(file_path, 'rb') as f:
        data = pickle.load(f)
    return data

class CodeDataset(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return torch.FloatTensor(self.data[idx]['code']), torch.tensor(self.data[idx]['label'], dtype=torch.long)

def train(model, train_loader, criterion, optimizer, device):
    model.train()
    total_loss = 0
    correct = 0
    for batch_idx, (data, labels) in enumerate(train_loader):
        data, labels = data.to(device), labels.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, labels)
        total_loss += loss.item()
        loss.backward()
        optimizer.step()
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(labels.view_as(pred)).sum().item()
    return total_loss / len(train_loader), correct / len(train_loader.dataset)

def validate(model, val_loader, criterion, device):
    model.eval()
    total_loss = 0
    correct = 0
    with torch.no_grad():
        for data, labels in val_loader:
            data, labels = data.to(device), labels.to(device)
            output = model(data)
            loss = criterion(output, labels)
            total_loss += loss.item()
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(labels.view_as(pred)).sum().item()
    return total_loss / len(val_loader), correct / len(val_loader.dataset)

def main():
    device = "cpu"

    # Load data
    data_path = "./parser/parser_results/data.pkl"
    data = load_data(data_path)

    # Model parameters
    input_dim = len(data[0]['code'])
    hidden_dim = 40
    encode_dim = 64
    label_size = 3
    batch_size = 64

    # Initialize model, criterion, and optimizer
    model = BatchProgramClassifier(input_dim, hidden_dim, encode_dim, label_size, batch_size).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    # K-fold cross-validation
    k = 5  # Number of folds
    kf = KFold(n_splits=k, shuffle=True, random_state=42)

    for fold, (train_indices, val_indices) in enumerate(kf.split(data)):
        print(f"Fold {fold+1}/{k}")
        train_data = [data[i] for i in train_indices]
        val_data = [data[i] for i in val_indices]
        test_size = 0.2
        val_data, test_data = train_test_split(val_data, test_size=test_size, random_state=42)


        train_dataset = CodeDataset(train_data)
        val_dataset = CodeDataset(val_data)
        test_dataset = CodeDataset(test_data)  # Add test dataset
        train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
        val_loader = DataLoader(val_dataset, batch_size=32, shuffle=False)
        test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)  # Add test loader

        # Training loop
        num_epochs = 15
        for epoch in range(num_epochs):
            train_loss, train_acc = train(model, train_loader, criterion, optimizer, device)
            val_loss, val_acc = validate(model, val_loader, criterion, device)
            print(f"Epoch {epoch+1}/{num_epochs}: Train Loss {train_loss:.4f}, Train Acc {train_acc:.4f}, Val Loss {val_loss:.4f}, Val Acc {val_acc:.4f}")

    # Testing
    test_loss, test_acc = validate(model, test_loader, criterion, device)
    print(f"Testing Loss {test_loss:.4f}, Testing Acc {test_acc:.4f}")



if __name__ == "__main__":
    main()
