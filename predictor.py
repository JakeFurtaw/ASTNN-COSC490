# predictor.py
import torch
from transformers import RobertaTokenizerFast, RobertaForSequenceClassification

# Load the tokenizer and model
tokenizer = RobertaTokenizerFast.from_pretrained('microsoft/codebert-base')
model = RobertaForSequenceClassification.from_pretrained('./results/checkpoint-1188', num_labels=2)

def predict_code(code_str):
    # Convert the code to the format expected by the model
    encoding = tokenizer.encode_plus(
        code_str,
        add_special_tokens=True,
        max_length=512,
        return_token_type_ids=False,
        padding='max_length',
        return_attention_mask=True,
        return_tensors='pt',
        truncation=True
    )


    # Get the model's prediction
    with torch.no_grad():
        outputs = model(**encoding)
        logits = outputs.logits

    # Convert the logits to probabilities and find the class with the highest probability
    probs = logits.softmax(dim=1)
    prediction = probs.argmax(dim=1)

    return prediction.item()

def get_code():
     # Prompt the user to enter a string
    user_string = input("Please enter a java method: ")

    # Store the string as a triple-quoted string
    triple_quoted_string = f'''{user_string}'''

    return triple_quoted_string

if __name__ == "__main__":
    # Test the function with a string of code
    
    while True:
        user_string = get_code()
        
        if user_string.lower() == 'exit':
            break
        else:
            prediction = predict_code(code_str)
            print(f"The code is predicted to be {'non-equivalent' if prediction == 1 else 'equivalent'}")
        
    print("Terminating....")
    
