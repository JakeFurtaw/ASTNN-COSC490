import pandas as pd
import pprint
from utils import *

#Removes non equivalent mutants from the dataset
# Load the pickle file
data = pd.read_pickle("parser_results/data.pkl")
print(data[0])

def sample(data, limit):
    noneq = 0
    eq = 0
    sample_data = []
    for line in data:
        if 0 == line['label'] and limit != 0:
            sample_data.append(line)
            limit-=1
            noneq+=1
        elif 1 == line['label']:
            sample_data.append(line)
            eq+=1
    print("Non-Equivalent: ", noneq)
    print("Equivalent: ", eq)
    print(f"Size of new sample: ",len(sample_data))
    return sample_data
            
new_data=sample(data, 1000)
list_to_pkl(new_data, './parser_results/', 'sampled_data.pkl')