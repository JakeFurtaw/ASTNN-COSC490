import os
from utils import *
from config import *
import re
import random
import string

class UnifiedDiffParser:
    def __init__(self, mutant, program=None, difference=None):
        self.mutant = mutant
        self.string_to_del = ""
        self.string_to_add = ""
        self.line_num_to_del = None
        self.line_num_to_del2 = None
        self.line_num_to_add = None
        self.line_num_to_add2 = None
        self.new_file_name = ''
        self.counter = 1
        self.parse_unified_diff()
  
    def parse_unified_diff(self):
        # split the difference into lines
        lines = self.mutant.unified_diff.strip().split("\n")
        self.line_num_to_del = int(lines[0].split(" ")[1].split(",")[0][1:])
        self.line_num_to_del2 = int(lines[0].split(" ")[1].split(",")[1][0:]) if len(lines[0].split(" ")[1].split(",")) > 1 else None
        self.line_num_to_add = int(lines[0].split(" ")[2].split(",")[0][1:])
        self.line_num_to_add2 = int(lines[0].split(" ")[2].split(",")[1][0:]) if len(lines[0].split(" ")[2].split(",")) > 1 else None
        # remove the first line
        lines = lines[1:]
   
        for i, line in enumerate(lines):
            if line.startswith("-"):
                self.string_to_del += line[1:]
                if i+1 < len(lines):
                    if lines[i+1].startswith("-"):
                        self.string_to_del += "\n"
            elif line.startswith("+"):
                self.string_to_add += line[1:]
                if i+1 < len(lines):
                    if lines[i+1].startswith("+"):
                        self.string_to_add += "\n"
            else:
                pass

    def parse_file(self, mutant_list, save_to_file, extract_methods):
        with open(f"{original_program_path}{self.mutant.program_name}", 'r') as f:
            file = iter(f)
            file_copy = list(file)

        #end_line = self.line_num_to_del + self.line_num_to_del2 if self.line_num_to_del2 is not None else self.line_num_to_add
        #del file_copy[self.line_num_to_add-1:end_line]
        #file_copy.insert(self.line_num_to_add - 1, self.string_to_add + '\n')

        if self.line_num_to_add2 is None and self.line_num_to_del2 is None and len(mutant_list) == 1:
            end_line = self.line_num_to_del2 if self.line_num_to_del2 is not None else self.line_num_to_del
            del file_copy[self.line_num_to_del-1:end_line]
            file_copy.insert(self.line_num_to_add-1, self.string_to_add + '\n')

            mod_str = ''.join(file_copy)
            
            if extract_methods:
                mod_str = extract_java_method(mod_str, self.line_num_to_del-1)

                # Remove single-line comments (//)
                mod_str = re.sub(r"//.*", "", mod_str)
                # Remove multi-line comments (/* ... */)
                mod_str = re.sub(r"/\*(.*?)\*/", "", mod_str, flags=re.DOTALL)
                mod_str = self.method_name_change(mod_str)
                if save_to_file:
                    self.save_str_to_file(mod_str)
            else:
                if save_to_file:
                    self.save_str_to_file(file_copy)
            return [mod_str]
        else:
            print('Ignoring mutant which diff lines > 1')
            return None

    def save_str_to_file(self, file_content, save_path=mutant_save_path):
        original_file_name = f"{self.mutant.program_name}.original"

        filename, extension = os.path.splitext(self.mutant.program_name)
        self.new_file_name = f"{filename}_{self.mutant.id_num}{extension}"
        
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        modified_file_path = os.path.join(save_path, self.new_file_name)

        while os.path.exists(modified_file_path):
            self.new_file_name = f"{filename}_{self.mutant.id_num}{extension}"
            modified_file_path = os.path.join(save_path, self.new_file_name)
            self.counter += 1

        if save_mutants_to_file:
            if file_content is not None:
                with open(modified_file_path, "w") as mod_file:
                    mod_file.write(''.join(file_content))
            else:
                print('skipped null list')
       
    def print_all(self):
        print(f'del {str(self.line_num_to_del)}')
        print(f'del2 {str(self.line_num_to_del2)}')
        print(f'add {str(self.line_num_to_add)}')
        print(f'add2 {str(self.line_num_to_add2)}')
        print(f'deleting: {self.string_to_del}')
        print(f'adding: {self.string_to_add}')
        print(f'file: {self.mutant.id_num}')
        print(f'diff: {self.mutant.unified_diff}')

    def method_name_change(self, java_method):
        # Find the opening parenthesis to locate the method name
        opening_parenthesis_index = java_method.index('(')
        
        # Extract the method name
        method_name = java_method[:opening_parenthesis_index].strip()

        # Add the number to the method name
        new_id_str = generate_unique_method_name()
        self.mutant.mathod_labeled = new_id_str

        modified_method_name = self.replace_last_word(method_name, new_id_str)

        # Replace the original method name with the modified method name
        modified_java_method = java_method.replace(method_name, modified_method_name, 1)    

        return modified_java_method

    def replace_last_word(self, string, replacement):
        words = string.split()
        if words:
            # Storing the original method name
            self.mutant.method = words[-1]
            words[-1] = replacement
        return ' '.join(words)

def generate_unique_method_name():
    unique_id = ''.join(random.choices(string.ascii_letters, k=10))
    return unique_id.lower()

