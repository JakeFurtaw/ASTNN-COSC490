import pandas as pd
import pprint
from utils import *

# Load the pickle file
data = pd.read_pickle("parser_results/AllOperators_v2.pkl")

def split_file_lines(file_path):
    # Read the file
    with open(file_path, 'r') as file:
        lines = file.readlines()

    dictionary = {}
    # Process each line
    for line in lines:
               # Split each line into key and values
        parts = line.split()
        key = parts[0]
        values = [float(value) for value in parts[1:]]

        # Add the key and values to the dictionary
        if key in dictionary:
            dictionary[key].extend(values)
        else:
            dictionary[key] = values
    return dictionary

my_dictionary = split_file_lines('method_embeddings.txt')

def order_data(data, my_dictionary):
    final_data = []
    for key in my_dictionary:
        for line in data:
            if key in line['code']:
                final_data.append({'id': line['id'], 'code': my_dictionary[key], 'operator': line['operator'], 'label': line['label'] -1, 'program': line['method'], 'method_name': line['method_labeled']})
    
    
    list_to_pkl(final_data, './parser_results/', 'data.pkl')
    list_to_csv(final_data, './parser_results/', 'data.csv')

       

order_data(data, my_dictionary)

