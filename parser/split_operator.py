import pandas as pd
import pprint
from utils import *

# Load the pickle file
data = pd.read_pickle("parser_results/data.pkl")

def operator_separator(operator, data):
    split_data = []
    for line in data:
        if operator in line['operator']:
            split_data.append(line)
    return split_data

operator = 'AOIS'
new_data = operator_separator(operator, data)
file_name = operator + '.pkl'
list_to_pkl(new_data, './parser_results/', file_name)
print(len(new_data))
print(new_data[0])